# Coursera-Data-Science

## Applied Data Science with Python Specialization

[Introduction to Data Science in Python](https://www.coursera.org/learn/python-data-analysis/home/welcome)

[Lab1](https://azddnjgn.labs.coursera.org/tree)

[Applied Plotting, Charting & Data Representation in Python](https://www.coursera.org/learn/python-plotting/home/welcome)

[Lab2](https://uhmkouwe.labs.coursera.org/tree)

[Applied Machine Learning in Python](https://www.coursera.org/learn/python-machine-learning/home/welcome)

[Lab3](https://kfohkogm.labs.coursera.org/tree)

[User Guide — pandas documentation](https://pandas.pydata.org/docs/user_guide/index.html)

## Deep Learning Specialization

[Stanford CS230 Deep Learning](https://cs230.stanford.edu/syllabus/#syllabus)

## DeepLearning.AI TensorFlow Developer Professional Certificate

[TensorFlow tutorials](https://www.tensorflow.org/tutorials)

[Google TensorFlow Certificate exam](https://www.tensorflow.org/certificate)

[Stanford CS 20: Tensorflow for Deep Learning Research](https://web.stanford.edu/class/cs20si/syllabus.html)

## Stanford Data Science Courses

[CS224n: NLP with Deep Learning](https://web.stanford.edu/class/cs224n/index.html#schedule)

[CS231n: CNN for Visual Recognition](http://cs231n.stanford.edu/schedule.html)

[CS234: Reinforcement Learning](https://web.stanford.edu/class/cs234/modules.html)

[CS 168: The Modern Algorithmic Toolbox](https://web.stanford.edu/class/cs168/)

[CS221: Artificial Intelligence: Principles and Techniques](https://cs221.stanford.edu/)

[CS 228 - Probabilistic Graphical Models](https://cs228.stanford.edu/)

[CS229: Machine Learning](https://cs229.stanford.edu/syllabus.html)

[CS229M: Statistical Learning Theory](https://web.stanford.edu/class/stats214/)

[CS236: Deep Generative Models](https://deepgenerativemodels.github.io/syllabus.html)

[CS236G Generative Adversarial Networks (GANs)](https://cs236g.stanford.edu/#lecture-schedule-)

[CS246: Mining Massive Data Sets](https://web.stanford.edu/class/cs246/index.html#schedule)

[CS131 Computer Vision: Foundations and Applications](https://cs131.stanford.edu/) &nbsp;
[cs131_notes](https://github.com/StanfordVL/cs131_notes)

[CS231A: Computer Vision, From 3D Reconstruction to Recognition](https://web.stanford.edu/class/cs231a/syllabus.html) &nbsp;
[cs231a-notes](https://github.com/kenjihata/cs231a-notes)

## Financial Engineering and Risk Management

[IEOR E4525: Machine Learning for OR & FE](https://martin-haugh.github.io/teaching/ml-orfe/)

[IEOR E4703: Monte Carlo Simulation](https://martin-haugh.github.io/teaching/monte-carlo)

[IEOR E4706: Foundations of Financial Engineering](https://martin-haugh.github.io/teaching/foundations-fe)

[IEOR E4707: Financial Engineering: Continuous Time Models](https://martin-haugh.github.io/teaching/cts-time-models)